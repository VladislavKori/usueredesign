// ==UserScript==
// @name         USUE REDESIGN
// @namespace    https://portal.usue.ru/portal
// @version      0.1
// @description  See nice!
// @author       VladislavKroi
// @match        https://portal.usue.ru/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=usue.ru
// @grant        none
// ==/UserScript==

(function () {
    'use strict';

    function connectFont() {
        var link1 = document.createElement("link");
        link1.setAttribute('rel', 'preconnect')
        link1.setAttribute('href', 'https://fonts.googleapis.com')
        document.head.appendChild(link1);

        var link2 = document.createElement("link");
        link2.setAttribute('rel', 'preconnect')
        link2.setAttribute('href', 'https://fonts.gstatic.com')
        link2.crossOrigin = true;
        document.head.appendChild(link2);

        var link = document.createElement('link');
        link.setAttribute('rel', 'stylesheet');
        link.setAttribute('href', "https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500&display=swap");
        document.head.appendChild(link);
    }

    const html = document.querySelector("html");
    html.style.cssText = `
        height: 100vh;
    `

    const body = document.body;
    body.style.cssText = `
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        height: 100%;
        font-family: 'Inter', sans-serif;
        background: linear-gradient(45deg, rgba(231,230,255,1), rgba(255,255,255,1));
    `

    function loginPage() {
        const portalContainer = document.querySelector("#portalContainer");
        if (portalContainer !== null) {
            portalContainer.style.cssText = `
              height: 100%;
            `
        }


        const headerMin = document.querySelector("#headerMin");
        if (headerMin !== null) {
            headerMin.style.cssText = `
            border: none;
            overflow: inherit;
            margin: 0;
            display: flex;
            justify-content: center;
            align-items: center;
           `
        }

        const mastHead = document.querySelector("#mastHead");
        if (mastHead !== null) {
            mastHead.style.cssText = `
           display: flex;
           flex-direction: column;
           align-items: center;
        `
        }

        const mastBanner = document.querySelector("#mastBanner")
        if (mastBanner !== null) {
            mastBanner.remove();
        }

        const portalOuterContainer = document.querySelector("#portalOuterContainer")
        if (portalOuterContainer !== null) {
            portalOuterContainer.style.cssText = `
                height: 100%;
            `
        }

        const loginForm = document.querySelector("#loginForm")
        if (loginForm !== null) {
            loginForm.style.cssText = `
                display: flex;
                flex-direction: column;
                align-items: flex-start;
                margin: 0;
            `
        }

        const labels = document.querySelectorAll("#mastLogin label");
        if (labels !== null && labels.length !== 0) {
            labels[0].innerText = "ID пользователя";
            labels[1].innerText = "Пароль";
            labels.forEach(item => {
                item.style.cssText = `
                   font-family: 'Inter', sans-serif;
                   font-size: 18px;
                   padding: 0;
                   margin-top: 18px;
                `
            })
        }

        const inputs = document.querySelectorAll("#mastLogin input");
        if (inputs !== null && labels.length !== 0) {
            inputs[0].placeholder = "Type Id";
            inputs[1].placeholder = "Type Password";
            inputs.forEach(item => {
                item.style.cssText = `
                    font-family: 'Inter', sans-serif;
                   font-size: 18px;
                   padding: 18px 22px;
                   border-radius: 20px;
                   background: white;
                   outline: none;
                   border: none;
                   font-width: 500;
                   margin: 0;
                   width: 300px;
                   margin-top: 6px;
                `
            })
        }

        const btn = document.querySelector("input#submit")
        if (btn !== null) {
            btn.style.cssText = `
                margin-top: 24px;
                background: #0B54A0;
                color: #fff;
                font-family: 'Inter', sans-serif;
                font-weight: 500;
                font-size: 18px;
                border: none;
                outline: none;
                border-radius: 20px;
                width: 100%;
                padding: 14px;
                cursor: pointer;
                transition: all .2s ease-in-out;
            `

            btn.value = "Sign In"

            btn.onmouseover = () => {
                btn.style.background = "#2889EE"
            }

            btn.onmouseleave = () => {
                btn.style.background = "#0B54A0"
            }
        }

        const logo = document.querySelector("#mastLogo img");
        if (logo !== null) {
            logo.style.cssText = `mix-blend-mode: multiply;`
        }

        const container = document.querySelector("#container");
        if (container !== null) {
            container.remove();
        }
    }

    function mainPage() {
        const col1of2 = document.querySelector("#col1of2")
        if (col1of2) { col1of2.remove(); }

        const toolMenuLinks = document.querySelectorAll("#toolMenu ul li a");
        if (toolMenuLinks !== null && toolMenuLinks.length !== 0) {
            toolMenuLinks.forEach(item => {
                item.style.cssText = `
                    text-decoration: none;
                `
            })
        }

        const toolMenuText = document.querySelectorAll("#toolMenu ul li a span");
        if (toolMenuText !== null && toolMenuText.length !== 0) {
            toolMenuText.forEach(item => {
                item.style.cssText = `
                    color: black;
                    font-family: 'Inter', sans-serif;
                    font-weight: 500;
                    font-size: 14px;
                `
            })
        }

        const portletTitleWraps = document.querySelectorAll("#col2of2 .portletTitleWrap");
        if (portletTitleWraps !== null) {
            portletTitleWraps.forEach(item => {
                item.remove()
            })
        }

        const portletMainWraps = document.querySelectorAll("#col2of2 .portletMainWrap");
        if (portletMainWraps !== null && portletMainWraps[0] && portletMainWraps[1] && portletMainWraps[2]) {
            portletMainWraps[0].remove()
            portletMainWraps[1].remove()

            // portletMainWraps.forEach(item => item.style.display = "none")
        }

        const col2of2 = document.querySelector("#col2of2");
        if (col2of2 !== null) {
            col2of2.style.cssText = `
                width: 100%;
                float: inherit;
            `
        }

        const siteNavWrapper = document.querySelector("#siteNavWrapper")
        if (siteNavWrapper) { siteNavWrapper.remove() }

        const headerMax = document.querySelector("#headerMax");
        if (headerMax) { headerMax.style.cssText = `margin: 0;` }

        const col1PortletMainWrap = document.querySelector("#col1 .portletMainWrap")
        if (col1PortletMainWrap !== null) {
            col1PortletMainWrap.style.height = '90vh'
        }

        const iframe = document.querySelector(".portletMainIframe");
        if (iframe !== null) {
            setTimeout(() => {
                iframe.style.height = "100%"
            }, 400)
        }
    }

    function removeFooter() {
        const footer = document.querySelector("#footer")
        if (footer !== null) {
            footer.remove();
        }
    }

    const loginLink1 = document.querySelector("#loginLink1");
    if (loginLink1 !== null) {
        connectFont();
        removeFooter();
        mainPage();
    } else {
        connectFont();
        removeFooter();
        loginPage();
    }
})();